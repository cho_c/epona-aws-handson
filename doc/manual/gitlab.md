# GitLabのアカウント作成

## アカウント作成

下記サイトを参考に、**GitLabのアカウント作成** の全手順を実施ください。  
登録したユーザー名/パスワードを忘れないよう、必要に応じてメモをとるなどの対応をしてください。

また、パーソナルアクセストークンは研修中利用しますので、必ずメモを取ってください。

**[GitLab アカウントの作成方法](https://www.gitlab.jp/blog/2020/06/23/steps-to-get-a-gitlab-dot-com-account-and-get-set-up/)**

GitLabのアカウントを既にお持ちの方は、以下項目について必ず確認し、必要に応じてご対応ください。

* 2要素認証の登録
* パーソナルアクセストークンを生成する

## Configの確認

以下コマンドで、利用したいアカウントの設定であるかを確認してください。

```shell script
$ git config --get user.name
$ git config --get user.email
```

もし異なる設定が表示された場合、以下コマンドを実行し再設定してください。

```shell script
$ git config --global user.name "[名前]"
$ git config --global user.email "[メールアドレス]"
```
