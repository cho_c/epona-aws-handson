data "terraform_remote_state" "staging_public_traffic_container_service_backend" {
  backend = "s3"

  config = {
    bucket         = "suzuki-staging-terraform-tfstate"
    key            = "public_traffic_container_service_backend/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "suzuki_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/SuzukiStagingTerraformBackendAccessRole"
  }
}
