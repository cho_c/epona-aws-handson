# TODO:AWSプロバイダーを使うための設定を修正
provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/SuzukiTerraformExecutionRole"
  }
}

# TODO:module名を"cd_pipeline_backend_backend"に変更
module "cd_pipeline_backend_backend" {
  # TODO: 参照するEponaモジュールのバージョンを記載する
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend?ref=v0.2.1"

  # TODO: DeliveryアカウントIDに変更
  delivery_account_id = "938285887320"
  # TODO: backend向けECRリポジトリ名に変更。
  # TODO: タグをlatestに変更。また、チャットアプリケーションであるためアーティファクト名を chat に変更
  ecr_repositories = {
    suzuki-chat-example-backend = {
      tag            = "latest" # リリース対象タグ
      artifact_name  = "chat"
      container_name = "IMAGE1_NAME"
    }
  }

  # Delivery環境のECRにCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_backend_triggerの output として出力される
  # TODO: cd_pipeline_backend_trigger_backend の output として出力されたARNに変更
  cross_account_codepipeline_access_role_arn = "arn:aws:iam::938285887320:role/Suzuki-Pipeline-BackAccessRole"
  # TODO: cd_pipeline_backend_trigger_backend の bucket_name と同じ名前を指定する
  source_bucket_name = "suzuki-pipeline-back-source"

  # TODO: backend向けのデータを参照するため、 staging_public_traffic_container_service_backend に変更
  cluster_name = data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service_backend.ecs_cluster_name
  service_name = data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service_backend.ecs_service_name

  # TODO: pipeline_name, artifact_store_bucket_name をわかりやすいリソース名となるように変更
  pipeline_name                       = "suzuki-pipeline-back"
  artifact_store_bucket_name          = "suzuki-back-artfct"
  artifact_store_bucket_force_destroy = true   # TODO: ハンズオンではS3バケットの削除保護を無効化するため、trueに変更

  # TODO: backend向けのデータを参照するため、 staging_public_traffic_container_service_backend に変更
  prod_listener_arns = [data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service_backend.load_balancer_prod_listener_arn]
  target_group_names = data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service_backend.load_balancer_target_group_names

  # ECSへのデプロイに使用するデプロイメントグループ名やCodeDeployのアプリケーション名をわかりやすいリソース名となるように変更
  codedeploy_app_name         = "suzuki-chat-backend"
  deployment_group_name_ecs   = "suzuki-chat-backend-group"

  # TODO: ハンズオンではデプロイを承認不要とする
  deployment_require_approval = false
}
