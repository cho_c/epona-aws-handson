data "terraform_remote_state" "delivery_network" {
  backend = "s3"

  config = {
    # TODO: bucketの先頭[last_name]を修正
    bucket  = "suzuki-delivery-terraform-tfstate"
    key     = "network/terraform.tfstate"
    encrypt = true
    # TODO: dynamodb_tableの先頭[last_name]を修正
    dynamodb_table = "suzuki_terraform_tfstate_lock"

    # TODO: role_arnのロール名 [LastName]を修正
    role_arn = "arn:aws:iam::938285887320:role/SuzukiDeliveryTerraformBackendAccessRole"
  }
}
