data "terraform_remote_state" "delivery_ci_pipeline" {
  backend = "s3"
  config = {
    bucket         = "suzuki-delivery-terraform-tfstate"
    key            = "ci_pipeline/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "suzuki_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/SuzukiDeliveryTerraformBackendAccessRole"
  }
}
