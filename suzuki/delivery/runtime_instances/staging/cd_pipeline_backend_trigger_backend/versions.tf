terraform {
  # Terraform は、0.13.5 以上のバージョンを利用する
  required_version = "0.13.5"

  # 本パターンを適用するための aws provider のバージョンを指定する
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.18.0"
    }
  }

  backend "s3" {
    bucket         = "suzuki-delivery-terraform-tfstate"
    key            = "staging/cd_pipeline_backend_trigger_backend/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "suzuki_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/SuzukiDeliveryTerraformBackendAccessRole"
  }
}
