# TODO:AWSプロバイダーを使うための設定を修正
provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::938285887320:role/SuzukiTerraformExecutionRole"
  }
}

# TODO:Runtime環境のアカウントIDに変更
locals {
  runtime_account_id = "922032444791"
}

data "aws_region" "current" {}

# TODO:区別をするため、module名を"cd_pipeline_backend_trigger_backend"に変更
module "cd_pipeline_backend_trigger_backend" {
  # TODO:参照するEponaモジュールのバージョンを記載する
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend_trigger?ref=v0.2.1"

  # TODO:パイプライン名とデプロイ用設定ファイルを配置するAmazon S3バケット名を変更
  name                 = "suzuki-pipeline-back"
  bucket_name          = "suzuki-pipeline-back-source"
  bucket_force_destroy = true   # TODO: ハンズオンではS3バケットの削除保護を無効化するため、trueに変更
  runtime_account_id   = local.runtime_account_id

  # TODO:リポジトリ名と、その ARN およびデプロイ対象タグのマップ。ARNを記載。
  ecr_repositories = {
    "suzuki-chat-example-backend" = {
      arn  = data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.container_image_repository_arns["suzuki-chat-example-backend"]
      tags = ["latest"]
    }
  }

  target_event_bus_arn      = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"
  # TODO:CodePipeline が用いる artifact store のバケット ARNを指定
  # Runtime環境上に作成するバケット名をbucket_nameとすると、arn:aws:s3:::bucket_name の形式で記載してください
  # see: https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/dev/s3-arn-format.html
  artifact_store_bucket_arn = "arn:aws:s3:::suzuki-back-artfct"

  # TODO: artifact_store_bucket_encryption_key_arn のパラメータをコメントアウト
  # 以下は Runtime環境上で cd_pipeline_backend_backend を動かしてから設定する
  # artifact_store_bucket_encryption_key_arn = "arn:aws:kms:${data.aws_region.current.name}:${local.runtime_account_id}:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}
